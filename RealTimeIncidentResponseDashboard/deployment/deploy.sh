#!/bin/bash

# Define variables
SPLUNK_HOME=/opt/splunk
PROJECT_DIR=$(dirname "$(readlink -f "$0")")/..
DASHBOARDS_DIR=$PROJECT_DIR/dashboards
SCRIPTS_DIR=$PROJECT_DIR/src/scripts
ALERTS_DIR=$PROJECT_DIR/src/alerts
SPLUNK_ETC=$SPLUNK_HOME/etc/apps/my_splunk_app/local

# Check if SPLUNK_HOME exists
if [ ! -d "$SPLUNK_HOME" ]; then
    echo "Error: Splunk home directory does not exist."
    exit 1
fi

# Copy dashboard configurations
echo "Deploying dashboard configurations..."
cp $DASHBOARDS_DIR/*.xml $SPLUNK_ETC/dashboards/

# Copy scripts
echo "Deploying scripts..."
cp $SCRIPTS_DIR/*.sh $SPLUNK_ETC/scripts/

# Copy alert configurations
echo "Deploying alert configurations..."
cp $ALERTS_DIR/*.conf $SPLUNK_ETC/alerts/

# Restart Splunk to apply changes
echo "Restarting Splunk..."
$SPLUNK_HOME/bin/splunk restart

echo "Deployment completed successfully."
