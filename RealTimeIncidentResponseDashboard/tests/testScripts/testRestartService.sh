#!/bin/bash
# Test script for restartService.sh

# Simulate the restart service script without actually restarting a critical service
SERVICE_NAME="dummy_service"

# Call the restart service script
../src/scripts/restartService.sh $SERVICE_NAME

# Check exit status
if [[ $? -eq 0 ]]; then
    echo "Test Passed: $SERVICE_NAME restart simulated successfully."
else
    echo "Test Failed: $SERVICE_NAME restart simulation failed."
fi
