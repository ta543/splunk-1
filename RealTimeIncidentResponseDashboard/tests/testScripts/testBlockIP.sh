#!/bin/bash
# Test script for blockIP.sh

# Define a dummy IP to test IP blocking functionality
IP_ADDRESS="192.0.2.1"

# Call the block IP script
../src/scripts/blockIP.sh $IP_ADDRESS

# Check exit status
if [[ $? -eq 0 ]]; then
    echo "Test Passed: Blocking IP $IP_ADDRESS simulated successfully."
else
    echo "Test Failed: Blocking IP $IP_ADDRESS simulation failed."
fi
