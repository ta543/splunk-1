#!/bin/bash
# Script to restart a service. Assumes you have sudo privileges.

SERVICE_NAME=$1

echo "Attempting to restart the service: $SERVICE_NAME..."
sudo systemctl restart $SERVICE_NAME

if [[ $? -eq 0 ]]; then
    echo "$SERVICE_NAME restarted successfully."
else
    echo "Failed to restart $SERVICE_NAME."
fi
