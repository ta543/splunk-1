#!/bin/bash
# Script to block an IP address using iptables. Assumes you have sudo privileges.

IP_ADDRESS=$1

echo "Blocking IP address: $IP_ADDRESS..."
sudo iptables -A INPUT -s $IP_ADDRESS -j DROP

if [[ $? -eq 0 ]]; then
    echo "IP address $IP_ADDRESS blocked successfully."
else
    echo "Failed to block IP address $IP_ADDRESS."
fi
